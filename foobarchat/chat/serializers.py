from rest_framework import serializers

from django.contrib.auth.models import User
from .models import Conversation
from .models import Message
from core.serializers import ProfileSerializer

# Serializers define the API representation.
class UserSerializer(serializers.ModelSerializer):
	#profile = ProfileSerializer(many=False,read_only=True)
	class Meta:
		model = User
		fields = ('username', 'profile', 'first_name', 'last_name', 'pk')
		read_only_fields = ('username', 'profile', 'pk')

class MessageSerializer(serializers.ModelSerializer):
	#user = UserSerializer(many=False, read_only=True)
	class Meta:
		model = Message
		fields = ('user', 'modified', 'conversation', 'pk', 'content_type', 'text', 'file', 'image')
		read_only_fields = ('user', 'modified', 'pk')
	
	def validate(self, data):
		if not len(data['content_type']):
			raise serializers.ValidationError('No message type was supplied!')
		elif data['content_type'] == 'T':
			if not len(data['text']):
				raise serializers.ValidationError('Text type message but no text supplied!')
		elif data['content_type'] == 'I':
			pass
		elif data['content_type'] == 'F':
			pass
		return data

class ConversationSerializer(serializers.ModelSerializer):
	#users = UserSerializer(many=True, read_only=False)
	#message_set = MessageSerializer(many=True, read_only=True)
	last_active = serializers.FloatField(source='get_last_active', read_only=True)
	#last_message = serializers.IntegerField(source='get_last_message', read_only=True)
	class Meta:
		model = Conversation
		fields = ('users', 'name', 'last_active', 'modified', 'message_set', 'pk')
		read_only_fields = ('modified', 'message_set', 'pk')

	#def validate(self, data):
#		if len(data['users']) == 1:
#			raise serializers.ValidationError('Cannot create conversation with one user!')
from django.shortcuts import render
from django.shortcuts import get_object_or_404

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.mixins import UpdateModelMixin

from core.serializers import ProfileSerializer
from core.models import Profile

from .serializers import UserSerializer
from django.contrib.auth.models import User

from .serializers import ConversationSerializer
from .models import Conversation

from .serializers import MessageSerializer
from .models import Message

from core.serializers import FriendsSerializer

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	serializer_class = UserSerializer

	#def get_queryset(self):
	#	return User.objects.exclude(pk=self.request.user.pk)

	#def create(self, obj):
	#	obj

	#def get_queryset(self):
	#	return self.request.user.profile.friends.all()

class ProfileViewSet(viewsets.ModelViewSet):
	queryset = Profile.objects.all()
	serializer_class = ProfileSerializer

class ConversationViewSet(viewsets.ModelViewSet):
	#queryset = Conversation.objects.all()
	serializer_class = ConversationSerializer

	def get_queryset(self):
		return self.request.user.conversation_set.all()

class MessageViewSet(viewsets.ModelViewSet):
	#queryset = Message.objects.all()
	serializer_class = MessageSerializer

	def get_queryset(self):
		return Message.objects.filter(
			conversation__users__in=[self.request.user])

	#make sure the message is from the right user
	def perform_create(self, serializer):
		serializer.save(user=self.request.user)

class CurUserViewSet(viewsets.ReadOnlyModelViewSet):
	serializer_class = UserSerializer
	def get_queryset(self):
		return [self.request.user]

class FriendsViewSet(viewsets.ModelViewSet, UpdateModelMixin):
	serializer_class = FriendsSerializer
	def get_queryset(self):
		return self.request.user.profile.friends

	def perform_update(self, serializer):
		print(self)
		return serializer.save(pk=self.request.user.profile.pk)

from django.conf.urls import url, include
from rest_framework import routers
from .views import UserViewSet, CurUserViewSet, ProfileViewSet, ConversationViewSet, MessageViewSet, FriendsViewSet

router = routers.DefaultRouter()
router.register(r'user', UserViewSet,'user')
router.register(r'whoami', CurUserViewSet,'whoami')
router.register(r'profile', ProfileViewSet)
router.register(r'conversation', ConversationViewSet,'conversation')
router.register(r'message', MessageViewSet,'message')
#router.register(r'friends', FriendsViewSet,'friends')

urlpatterns = [
	url(r'^',include(router.urls))
]

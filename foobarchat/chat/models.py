from django.db import models
from django.contrib.auth.models import User
from core.models import BaseModel

# Create your models here.
class Conversation(BaseModel):
	#link users to conversations
	users = models.ManyToManyField(User)
	name = models.CharField(max_length=100, blank=True)

	def get_last_active(self):
		if self.message_set.count() == 0: return self.modified.timestamp()
		return self.message_set.latest('created').created.timestamp() or 0.0
	
	#can be handled clientside, so it shall
	#def get_last_message(self):
	#if self.message_set.count() == 0: return None
	#	return self.message_set.latest('created').pk


	class Meta:
		ordering = ('modified', )

	def __str__(self):
		return 'Conv: ' + str(self.users.all())


class Message(BaseModel):
	#link back to user who sent the message
	user = models.ForeignKey(User)
	#link messages back to a conversation
	conversation = models.ForeignKey(Conversation)
	#so we know what content type we are dealing with
	CONTENT_TYPES = (
		('T','Text'),
		('F','File'),
		('I','Image'),
	)
	content_type = models.CharField(max_length=1, choices=CONTENT_TYPES)
	#our actual content
	text = models.TextField(max_length=2000, blank=True)
	file = models.FileField(upload_to='uploads/', null=True, blank=True)
	image = models.ImageField(upload_to='imageUploads/', null=True, blank=True)

	#we use the inherited 'created' field
	#from BaseModel to act as our message sent time

	class Meta:
		ordering = ('created',)
	
	def __str__(self):
		return 'Msg: send:%s, recv: %s' % (self.user,set(self.conversation.users.all())-set([self.user]))
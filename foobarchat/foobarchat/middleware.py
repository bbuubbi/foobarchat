from django.shortcuts import HttpResponseRedirect

class AuthRequiredMiddleware(object):
	def __init__(self, get_response):
		self.get_response = get_response

	def __call__(self, request):
		#makes sure we boot a user to the login
		#screen if not logged in.
		response = self.get_response(request)
		if not (request.user.is_authenticated() or
		request.path.startswith('/account/') or
		request.path.startswith('/admin/')):
			return HttpResponseRedirect('/account/login')
		return response
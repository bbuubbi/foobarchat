import os
from django.views.generic.base import TemplateView
from django.http import HttpResponse
from django.shortcuts import render, HttpResponseRedirect
from .forms import UserForm, ProfileForm
from foobarchat import settings

# Create your views here.
class Home(TemplateView):

	template_name = "app.html"

	def get_context_data(self, **kwargs):
		context = super(Home, self).get_context_data(**kwargs)
		return context

class Register(TemplateView):

	template_name = "registration/register.html"

	def get_context_data(self, **kwargs):
		context = super(Register, self).get_context_data(**kwargs)
		context['aform'] = UserForm(prefix='a')
		context['bform'] = ProfileForm(prefix='b')
		return context

	def post(self, request):
		uf = UserForm(request.POST, prefix='a')
		pf = ProfileForm(request.POST, prefix='b')
		if uf.is_valid() and pf.is_valid():
			usr = uf.save()
			usr.profile.bio = pf.cleaned_data['bio']
			usr.profile.birth_date = pf.cleaned_data['birth_date']
			usr.profile.picture = pf.cleaned_data['picture']
			return HttpResponseRedirect('/')

		return render(request,self.template_name,
			{'aform':UserForm(prefix='a'),'bform':ProfileForm(prefix='b')})

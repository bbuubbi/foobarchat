from django.contrib.auth.models import User
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import Profile

class UserForm(UserCreationForm):
	def clean(self):
		return super(UserForm, self).clean()
	class Meta:
		password = forms.CharField(widget=forms.PasswordInput())
		model = User
		widgets = {
			'password': forms.PasswordInput(),
		}
		fields = ['username', 'first_name','last_name']

class ProfileForm(ModelForm):
	def clean(self):
		return super(ProfileForm, self).clean()
	class Meta:
		model = Profile
		exclude = ['user']
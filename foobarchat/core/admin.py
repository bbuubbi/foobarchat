from django.contrib import admin
from .models import BaseModel, Profile

# Register your models here.
admin.site.register(Profile)

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.

class BaseModel(models.Model):
	#baseclass so we can know some extra info if need be
	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True

class Profile(BaseModel):
	#user profile, linked 1-1, contains optional bio,bday,picture, and friends
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	bio = models.TextField(max_length=500, blank=True)
	birth_date = models.DateField(null=True, blank=True)
	picture = models.ImageField(upload_to='profilePictures/', null=True, blank=True)
	friends = models.ManyToManyField('self', blank=True)
	
	#i now know in hindsight this DB layout is SHIT for
	#actual network efficency, and yes, had i known about
	#abstractUser i would just create my own user with more
	#more sensible field layouts,
	#there's not enough time though.

	#signal to create profile object when creating user
	@receiver(post_save, sender=User)
	def update_user_profile(sender, instance, created, **kwargs):
		if created:
			Profile.objects.create(user=instance)
			instance.profile.save()

	class Meta:
		pass

	def __str__(self):
		return self.user.username
		
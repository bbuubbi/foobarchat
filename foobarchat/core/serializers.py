from rest_framework import serializers
from .models import Profile

class ProfileSerializer(serializers.ModelSerializer):

	class Meta:
		model = Profile
		fields = ('user', 'picture', 'pk')
		read_only_fields = ('user', 'pk')

class FriendsSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ('user','picture','pk')
		read_only_fields = ('picture','user','pk',)
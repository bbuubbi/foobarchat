require('../js/sidebar.js');
require('../js/chat.js');

require('../html/sidebar.html');
require('../html/chat.html');

require('../html/friends.html');
require('../html/conversations.html');
require('../html/user.html');
require('../html/msg.html');

'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
	'ngRoute',
	'ngResource',
	'ngCookies',
	'angularMoment',
	'luegg.directives',
	'myApp.chatView',
	'myApp.sidebarView',
	]).
config(['$locationProvider', '$routeProvider', '$httpProvider', function($locationProvider, $routeProvider, $httpProvider) {
	$locationProvider.hashPrefix('!');
	$routeProvider.otherwise({redirectTo: '/'});
	$httpProvider.defaults.xsrfCookieName = 'csrftoken';
	$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]).
filter('yesNo', function() {
	return function(input) {
		return input ? 'yes' : 'no';
	}
}).
factory('Whoami', function($resource) {
	return $resource(
		'/api/whoami/',
		{},
		{'update': { method:'PATCH' }},
		{stripTrailingSlashes: false}
		);
}).
factory('User', function($resource) {
	return $resource(
		'/api/user/:id/',
		{},
		{'update': { method:'PATCH' }},
		{stripTrailingSlashes: false}
		);
}).
factory('Profile', function($resource) {
	return $resource(
		'/api/profile/:id/',
		{},
		{'update': { method:'PATCH' }},
		{stripTrailingSlashes: false}
		);
}).
factory('Conversation', function($resource) {
	return $resource(
		'/api/conversation/:id/',
		{},
		{
			'update': { method:'PATCH' },
			'remove': { method:'DELETE' }
		},
		{stripTrailingSlashes: false}
		);
}).
factory('Message', function($resource) {
	return $resource(
		'/api/message/:id/',
		{},
		{'update': { method:'PATCH' }},
		{stripTrailingSlashes: false}
		);
}).
factory('Friends', function($resource) {
	return $resource(
		'/api/friends/:id/',
		{},
		{'update': { method:'PATCH' }},
		{stripTrailingSlashes: false}
		);
}).
factory('APIHelper',['$resource', 'User', 'Friends', 'Conversation', 'Profile', 'Message',
	function($resource, User, Friends, Conversation, Profile, Message) {
		var foo = {
		//make generalized data fetchers
		generic_get: function(ob, name, endp, f) {
			endp.get({'id':ob[name]}).$promise.then(function(data){
				if(f != undefined) { f(data); }
				ob[name] = data;
			});
		},

		gqry: function(ob, name, endp,id, f, ff) {
			endp.get({'id':id}).$promise.then(function(data) {
				if(f != undefined) { f(data); }
				ob[name].push(data);
				if(ff != undefined) { ff(ob[name]); }
			});
		},

		generic_query: function(ob, name, endp, f, ff) {
			var arr = ob[name];
			var ii = 1;
			ob[name] = [];
			for(var i = 0; i < arr.length; i++) {
				endp.get({'id':arr[i]}).$promise.then(function(data) {
					if(f != undefined) { f(data); }
					ob[name].push(data);
					if(ff != undefined && ii == arr.length) { ff(ob[name]); }
					ii++;
				});
			}
		},

		//expand to more helpful helper functions (HAh :V)
		get_profiledata: function(ob,f) {
			foo.generic_get(ob, 'profile', Profile, f);
		},

		get_userdata: function(ob,f) {
			foo.generic_get(ob, 'user', User, f);
		},

		get_userdatas: function(ob,f,ff) {
			foo.generic_query(ob, 'users', User, f,ff);
		},

		get_conversation: function(ob,f) {
			foo.generic_get(ob, 'conversation', Conversation, f);
		},

		get_messages: function(ob,f,ff) {
			foo.generic_query(ob, 'message_set', Message, f,ff);
		},

		get_message: function(ob,f) {
			foo.generic_get(ob, 'message', Message, f);
		},
	};
	return foo;
}]).
filter('limitToText', function () {
	return function (content, length, tail) {
		if (isNaN(length))
			length = 50;

		if (tail == undefined)
			tail = "...";

		if ((content.length <= length) ||
			(content.length - tail.length <= length)) {
			return content;
	}
	else {
		return String(content).substring(0, length-tail.length) + tail;
	}
};
});
'use strict';

angular.module('myApp.chatView', []).

controller('ChatCtrl', ['$scope', '$rootScope', '$anchorScroll', 'Message', 'APIHelper', 'Conversation',
	function($scope, $rootScope, $anchorScroll, Message, APIHelper, Conversation) {
		$scope.userlookup = {};
		$scope.editname = 0;
		$rootScope.$on('createUserLookup', function(event, conv) {
			$scope.userlookup = {};
			$scope.editname = 0;
			for (var i = 0; i < conv.length; i++) {
				$scope.userlookup[conv[i].pk] = i;
			}
		});

		$scope.getUser = function(pk) {
			return $rootScope.conversation.users[$scope.userlookup[pk]];
		};

		$scope.isUser = function(pk) {
			return $rootScope.user.pk == pk;
		};

		$scope.sendChat = function() {
			if($scope.text.length > 0){
				var m = new Message();
				m.text = $scope.text;
				m.content_type = 'T';
				m.conversation = $rootScope.conversation.pk;
				m.$save(function(obj){
					$rootScope.conversation.message_set.push(obj);
					$scope.text = '';
				});
			}
		};

		$scope.editName = function() {
			$scope.editname = !$scope.editname;
			if(!$scope.editname){
				Conversation.update(
					{id: $rootScope.conversation.pk },
					{name: $rootScope.conversation.name},
					function(obj) {
						$rootScope.$emit('updateConv', obj);
					}
					);
			}
		};

		function condenseResource(R,ign=-1) {
			var fr = [];
			var ii = 0;
			for(var i = 0; i < R.length; i++) {
				if(ign != i) {
					fr[ii] = R[i].user.pk;
					ii++;
				}

			}
			return fr;
		}

		$scope.leave = function() {
			var foo = {conversation: $rootScope.conversation.pk}
			APIHelper.get_conversation(foo,function(o) {
				var bar = o.users;
				if(bar.length == 1) {
					Conversation.remove({id: o.pk});
					return;
				}
				var indx = bar.indexOf($rootScope.user.pk);
				bar.splice(indx,1);
				Conversation.update(
					{id: $rootScope.conversation.pk},
					{users: bar}
					);
				$rootScope.conversation = false;
			});
		};

		$scope.add = function() {
			var foo = {conversation: $rootScope.conversation.pk}
			APIHelper.get_conversation(foo,function(o) {
				var usrs = o.users
				usrs.push($scope.add.user);
				Conversation.update({id: o.pk}, {users: usrs})
			});
		};
	}]);
'use strict';

angular.module('myApp.sidebarView', []).

controller('SidebarCtrl',
	['$scope','$rootScope', '$http', '$interval', 'Whoami', 'User', 'Friends', 'Conversation', 'Profile', 'Message', 'APIHelper',
	function($scope,$rootScope, $http, $interval, Whoami , User, Friends, Conversation, Profile, Message, APIHelper) {

		$scope.user = {};
		$rootScope.user = {};
		$scope.friend = {pk:0};

		$rootScope.$on('updateConv', function(event, conv) {
			for(var i = 0; i < $scope.convs.length; i++) {
				if($scope.convs[i].pk == conv.pk) {
					$scope.convs[i] = conv;
					break;
				}
			}
		});

	//find out who we are and put it in scope
	Whoami.query().$promise.then(function(data) {
		APIHelper.get_profiledata(data[0]);
		$scope.user = data[0];
		$rootScope.user = data[0];
	});

	$interval(function() {
		if($scope.page) {
			$scope.udConvs();
		} else {
			$scope.udFrnds();
		}
	},10000);

	//get our conversations
	$scope.udConvs = function() {
		$scope.page = 1;
		Conversation.query().$promise.then(function(data) {
			$scope.convs = data;
		});
	};
	$scope.udConvs();

	//get our friends
	$scope.udFrnds = function(){
		$scope.page = 0;
		User.query().$promise.then(function(data) {
			data.splice(data.map(function(o) { return o.pk; }).indexOf($scope.user.pk),1);
			$scope.friends = data
			for(var i = 0; i < $scope.friends.length; i++)
				APIHelper.get_profiledata($scope.friends[i]);
		});
	};

	$interval(function() {
		if($rootScope.conversation) {
			var foo = {conversation: $rootScope.conversation.pk};
			APIHelper.get_conversation(foo,function(o) {
				var l = o.message_set.length -
				$rootScope.conversation.message_set.length;
				for(var i = 0; i < l; i++) {
					var m = {message: o.message_set[o.message_set.length - i - 1]};
					APIHelper.get_message(m,function(ob) {
						console.log(ob);
						$rootScope.conversation.message_set.push(ob);
					});
				}
			});
		}
	},5000);

	function expand_conv(obj){
		APIHelper.get_messages(obj);
		APIHelper.get_userdatas(obj, APIHelper.get_profiledata, function(o) {
			$rootScope.$emit('createUserLookup', o);
		});
	}

	$scope.actChat = function(pk){
		$rootScope.conversation = pk;
		//passing rootscope to a funtion just feels wrong...
		APIHelper.get_conversation($rootScope, expand_conv);
	};

	$scope.actFriend = function(pk){
		var c = new Conversation();
		console.log(c);
		c.users = [$scope.user.pk, pk];
		c.$save(function(obj){
			$scope.convs.push(obj);
			$rootScope.conversation = obj;
			expand_conv(obj);
		});
	};

	$scope.getLastMsg = function(conv) {
		console.log(conv)
		if(conv.message) { return null; }
		if(conv.message_set.length==0) { return null; }
		conv.message = conv.message_set[conv.message_set.length - 1];
		APIHelper.get_message(conv,function(o) {
			APIHelper.get_userdata(o, APIHelper.get_profiledata);
		});
	};

	$scope.getUser = function(ob) { return ob; };

	$scope.isUser = function(ob) {
		if(!ob)
			return false;
		return $scope.user.pk == ob.pk;
	};

	function condenseFriends(ign=-1) {
		var fr = [];
		var ii = 0;
		for(var i = 0; i < $scope.friends.length; i++) {
			if(ign != i) {
				fr[ii] = $scope.friends[i].user.pk;
				ii++;
			}

		}
		return fr;
	}

	/*
	$scope.addFriend = function() {
		var fr = condenseFriends();
		fr[$scope.friends.friends.length+0] = $scope.friend.pk;
		Friends.update({id: $scope.user.pk}, {friends: fr},function(obj){
			$scope.udFrnds();
		});
	};

	$scope.removeFriend = function(pk) {
		var fr = condenseFriends(pk);
		new Friends(fr).update();
	};
	*/

}]);
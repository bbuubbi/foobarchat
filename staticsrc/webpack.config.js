var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'production');
var APP_DIR = path.resolve(__dirname, 'development');

var config = {
	entry: APP_DIR + '/js/index.js',
	output: {
		path: BUILD_DIR,
		filename: 'bundle.js'
	},

	module: {
		rules: [
		{
			test: /\.(html)$/,
			use: [
			{
				loader: "file-loader",
				options: {
					name: "[name].[ext]",
				},
			},
			{
				loader: "extract-loader",
			},
			{
				loader: "html-loader",
				options: {
					attrs: ["img:src", "link:href"],
					interpolate: true,
				},
			},
			],
		},
		{
			test: /\.css$/,
			loaders: [
			{
				loader: "style-loader",
			},
			{
				loader: "css-loader",
			},
			],
		},
		{
			test: /\.jpg$/,
			loaders: [
			{
				loader: "file-loader"
			},
			],
		},
		{
			test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
			use: [
			{
				loader: 'url-loader',
				options: {
					limit: 10000,
					mimetype: 'application/font-woff'
				}
			}
			]
		},
		{
			test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
			use: [
			{ loader: 'file-loader' }
			]
		}
		]
	}
};

module.exports = config;